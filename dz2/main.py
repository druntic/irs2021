# -*- coding: utf-8 -*-
"""
Created on Sun Mar 14 17:20:22 2021

@author: David
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon

def init_plot(r_from=-2, r_to=2):
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlim(r_from, r_to)
    ax.set_ylim(r_from, r_to)
    ax.set_aspect("equal")
    # mreža (ax.set_xticks, ...)
    plt.grid()
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    return fig, ax


def R(theta):
    return np.array([
        [np.cos(theta), np.sin(theta), 0],
        [-np.sin(theta), np.cos(theta), 0],
        [0, 0, 1]
    ], dtype=float)

if __name__ == "__main__":
    fig, ax = init_plot()
    plt.draw()

    # inicijalno stanje robota
    i_xi = np.array([0, 0, 0], dtype=float)
    # stanje robota u gibajućem koordinatnom sustavu uvijek [0, 0, 0]
    l = 0.32
    r = 0.05

    # duljina šasije
    d = 0.1
    # širina kotača
    wheel_w = 0.03


    alpha1=np.pi/5
    alpha2=-alpha1
    alpha3=np.pi
    beta1=-alpha1
    beta2=-alpha2+np.pi*13/10
    beta3=0
    dt=0.01
    i=0
    wheel_points = [
        np.array([-wheel_w/2, -wheel_w/2, 0], dtype=float),
        np.array([-wheel_w/4, -wheel_w/2, 0], dtype=float),
        np.array([-wheel_w/4, -wheel_w/2-0.04, 0], dtype=float),
        np.array([-wheel_w/2, -wheel_w/2-0.04, 0], dtype=float),
        np.array([-wheel_w/2, -wheel_w/2-(d+r), 0], dtype=float),
        np.array([wheel_w/2, -wheel_w/2-(d+r), 0], dtype=float),
        np.array([wheel_w/2, -wheel_w/2-0.04, 0], dtype=float),
        np.array([wheel_w/4, -wheel_w/2-0.04, 0], dtype=float),
        np.array([wheel_w/4, -wheel_w/2, 0], dtype=float),
        np.array([wheel_w/2, -wheel_w/2, 0], dtype=float),
        np.array([wheel_w/2, wheel_w/2, 0], dtype=float),
        np.array([-wheel_w/2, wheel_w/2, 0], dtype=float)
    ]

    chassis_points = [
        np.array([l*np.cos(alpha1), -l*np.sin(alpha1), 0]),
        np.array([l*np.cos(alpha1), l*np.sin(alpha1), 0]),
        np.array([-l, l*np.sin(alpha1), 0]),
        np.array([-l, -l*np.sin(alpha1), 0]),
    ]


    wheel_l = np.array([
        R(i_xi[2]).transpose() @ R(alpha2).transpose() @ (R(beta2).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
    ])

    wheel_r = np.array([
        R(i_xi[2]).transpose() @ R(alpha1).transpose() @ (R(beta1).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
    ])

    chassis = np.array([
        R(i_xi[2]).transpose() @ point + np.array([i_xi[0], i_xi[1], 0]) for point in chassis_points
    ])
    wheel_m = np.array([
        R(i_xi[2]).transpose() @ R(alpha3).transpose() @ (R(beta3).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
    ])

    wheel_l_patch = ax.add_patch(Polygon(wheel_l[:, :2], facecolor="#000000"))
    wheel_r_patch = ax.add_patch(Polygon(wheel_r[:, :2], facecolor="#000000"))
    wheel_m_patch = ax.add_patch(Polygon(wheel_m[:, :2], facecolor="#000000"))
    chassis_patch = ax.add_patch(Polygon(chassis[:, :2], facecolor="#0000ff88"))
    v_dot = 0.5
    omega = np.pi/2

    while True:
        #

        M=np.array([
            [np.sin(alpha1+beta1),-np.cos(alpha1+beta1),-l*np.cos(beta1)],
            [np.sin(alpha2+beta2),-np.cos(alpha2+beta2),-l*np.cos(beta2)],
            [np.sin(alpha3+beta3),-np.cos(alpha3+beta3),-l*np.cos(beta3)],
            [np.cos(alpha1+beta1), np.sin(alpha1+beta1), d+l*np.sin(beta1)],
            [np.cos(alpha2+beta2), np.sin(alpha2+beta2), d+l*np.sin(beta2)],
            [np.cos(alpha3+beta3), np.sin(alpha3+beta3), d+l*np.sin(beta3)],
            ])
        
        phi_dots=M@R(i_xi[2]) @ np.array([v_dot * np.cos(i_xi[2]), v_dot * np.sin(i_xi[2]), omega], dtype=float)
        i_xi_dots=R(i_xi[2]).transpose() @ np.linalg.inv(M.transpose()@M)@M.transpose() @ phi_dots
        i_xi+=i_xi_dots*dt
        #print(-phi_dots[3]/d *dt)
        
        beta1+=-phi_dots[3]/d *dt
        beta2+=-phi_dots[4]/d *dt
        beta3+=-phi_dots[5]/d *dt
        #print(i_xi)
        #

        wheel_l = np.array([
            R(i_xi[2]).transpose() @ R(alpha2).transpose() @ (
                        R(beta2).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point
            in wheel_points
        ])

        wheel_r = np.array([
            R(i_xi[2]).transpose() @ R(alpha1).transpose() @ (
                        R(beta1).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point
            in wheel_points
        ])

        chassis = np.array([
            R(i_xi[2]).transpose() @ point + np.array([i_xi[0], i_xi[1], 0]) for point in chassis_points
        ])
        #print(i_xi[0])
        wheel_m = np.array([
            R(i_xi[2]).transpose() @ R(alpha3).transpose() @ (
                        R(beta3).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point
            in wheel_points
        ])
        #print(i_xi_dots)
        wheel_l_patch.xy = wheel_l[:, :2]
        wheel_r_patch.xy = wheel_r[:, :2]
        chassis_patch.xy = chassis[:, :2]
        wheel_m_patch.xy = wheel_m[:, :2]
        fig.canvas.draw_idle()
        plt.pause(dt)

        i += 1
        if i == 100:
            v_dot = 0.5
            omega = 0
        if i == 200:
            omega=-np.pi
        if i == 350:
            break
