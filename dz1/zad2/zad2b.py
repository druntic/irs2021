# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 20:03:56 2021

@author: David
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 12:10:49 2021

@author: David
"""

import time
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def skew(omega):
    M = np.array([
        [0, -omega[2], omega[1]],
        [omega[2], 0, -omega[0]],
        [-omega[1], omega[0],0]
    ])
    return M

def R(omega,t):
    norm=np.linalg.norm(omega,2)
    m=np.linalg.matrix_power(skew(omega), 2)
    M=np.identity(len(omega))+np.sin(norm*t)*skew(omega)/norm+(1-np.cos(norm*t))*m/(norm*norm)
    return M

if __name__ == "__main__":

    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")

    ax.set_xlim(-5, 5)
    ax.set_ylim(-5, 5)
    ax.set_zlim(-5, 5)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")

    points_initial1 = np.array([
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1],
        [0, 0, 0]
    ], dtype=float)
    points_initial2 = np.array([
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1],
        [0, 0, 0]
    ], dtype=float)
    sc = ax.scatter(points_initial1[:, 0], points_initial1[:, 1], points_initial1[:, 2])
    #sc = ax.scatter(points_initial2[:, 0], points_initial2[:, 1], points_initial2[:, 2])

    dt = 0.01
    j = 0
    k = 0
   
    #drugi dio
    j_p_i1 = np.array([0, 0, 0], dtype=float)#ishodiste okvira i u odnosu na j
    j_p_i2 = np.array([0, 0, 0], dtype=float)
    j_p_i_dot1 = np.array([1, 1, 0], dtype=float)#vektor prema kojem se tocke krecu
    j_p_i_dot2 = np.array([-2, -3, 0], dtype=float)
    omega1 = np.array([2*np.pi, 0, 0], dtype=float)#kutna brzina
    omega2 = np.array([0, 8*np.pi, 0], dtype=float)
    points1 = np.zeros(points_initial1.shape, dtype=float)
    points2 = np.zeros(points_initial1.shape, dtype=float)

    j_R_i = np.eye(3, dtype=float)
  
    while True:
        j += 1
        k+=1
        #print(points1[3])
        j_p_i1 += j_p_i_dot1 * dt
        j_p_i2 += j_p_i_dot2 * dt
        j_R_i = R(omega1,k*dt)
        j_R_i2 = R(omega2,k*dt)
        #print(j_R_i)
        #Rder=skew(omega)*j_R_i
        for i in range(points_initial1.shape[0]):
            points1[i] = j_R_i @ points_initial1[i]+j_p_i1
            #print("za i=",i,"R*points",j_R_i @ points_initial[i])            
            points2[i] = j_R_i2 @ points_initial2[i]+j_p_i2
            points2[i] = j_R_i @ points2[i]+j_p_i1
            #time.sleep(5)
      
        if j == 100:
            break
        plt.pause(dt)
        points=np.concatenate((points1, points2), axis=0)
        sc._offsets3d = points[:, 0], points[:, 1], points[:, 2]
        fig.canvas.draw_idle()