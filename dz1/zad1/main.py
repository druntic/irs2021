# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 12:40:02 2021

@author: David
"""
import time
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def skew(omega):
    M = np.array([
        [0, -omega[2], omega[1]],
        [omega[2], 0, -omega[0]],
        [-omega[1], omega[0],0]
    ])
    return M

def R(omega,t):
    norm=np.linalg.norm(omega,2)
    m=np.linalg.matrix_power(skew(omega), 2)
    M=np.identity(len(omega))+np.sin(norm*t)*skew(omega)/norm+(1-np.cos(norm*t))*m/(norm*norm)
    return M

if __name__ == "__main__":

    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")

    ax.set_xlim(-5, 5)
    ax.set_ylim(-5, 5)
    ax.set_zlim(-5, 5)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")

    points_initial = np.array([
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1],
        [0, 0, 0]
    ], dtype=float)

    sc = ax.scatter(points_initial[:, 0], points_initial[:, 1], points_initial[:, 2])

    dt = 0.01
    j = 0
    k = 0
    j_p_i = np.array([0, 0, 0], dtype=float)

    j_p_i_dot = np.array([0, 0, 0], dtype=float)#vektor prema kojem se tocke krecu

    omega = np.array([0, 0.25 * np.pi, 0], dtype=float)#kutna brzina

    points = np.zeros(points_initial.shape, dtype=float)

    j_R_i = np.eye(3, dtype=float)
    
    while True:
        j += 1
        k+=1
        #print(j)
        j_p_i += j_p_i_dot * dt
        j_R_i = R(omega,k*dt)
        #print(j_R_i)
        #Rder=skew(omega)*j_R_i
        for i in range(points_initial.shape[0]):
            points[i] = j_R_i @ points_initial[i]+j_p_i
            #print("za i=",i,"R*points",j_R_i @ points_initial[i])            
         
        if j == 100:
            omega = np.array([np.pi, 0, 0], dtype=float)
            for i in range(points_initial.shape[0]):
                points_initial[i]=points[i]
            k=0
            #time.sleep(5)
      
        if j == 200:
            break
        plt.pause(dt)

        sc._offsets3d = points[:, 0], points[:, 1], points[:, 2]
        fig.canvas.draw_idle()


    
