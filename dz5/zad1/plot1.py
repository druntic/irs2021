# -*- coding: utf-8 -*-
"""
Created on Sat May  8 23:31:35 2021

@author: David
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Wedge, Circle
from shared import r_R_i_array
from sampler import sampler
from robot import Robot
from particle_filter import prediction, correction


class Plot:
    def __init__(self, robot, estimated_robot, landmarks, particles, r_from=-3.0, r_to=3.0):
        self.robot=robot
        self.estimated_robot=estimated_robot
        self.landmarks=landmarks
        self.particles=particles
        self.r_from=r_from
        self.r_to=r_to
        self.landmarks_phi = np.arctan2(self.landmarks[:, 1], self.landmarks[:, 0])
        self.r_v_dot = 0.6
        self.r_omega = np.pi
        plt.ion()
        fig, ax = plt.subplots(1)
    
        ax.set_xlim(self.r_from, self.r_to)
        ax.set_ylim(self.r_from, self.r_to)
        ax.set_aspect("equal")
        ax.set_xticks(np.arange(self.r_from, self.r_to, 0.5))
        ax.set_yticks(np.arange(self.r_from, self.r_to, 0.5))
        ax.set_axisbelow(True)
        plt.grid()
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        self.fig=fig
        self.ax=ax
        
        #landmarks = np.random.rand(7, 2)
        landmarks_phi = np.arctan2(self.landmarks[:, 1], self.landmarks[:, 0])
        for landmark in self.landmarks:
            ax.add_patch(
                Circle(xy=landmark, radius=0.1, facecolor="#f4511e88")
            )
        M = 1000
        #particles = np.random.randn(M, 3)
        self.particles[:, 2] = np.arctan2(np.sin(self.particles[:, 2]), np.cos(self.particles[:, 2]))
        #self.particles_scatter = ax.scatter(self.particles[:, 0], self.particles[:, 1], s=0.5, c="#ff1744")

        plt.show()
        
        mean = 1 / M * np.sum(self.particles, axis=0)
        #print(mean)
        mean[2] = np.arctan2(np.sin(mean[2]), np.cos(mean[2]))
        #er = Robot(mean)
        
        self.particles_scatter = ax.scatter(self.particles[:, 0], self.particles[:, 1], s=0.5, c="#ff1744")
        
        self.robot_patch = ax.add_patch(
        Wedge(
            (self.robot.i_xi[0], self.robot.i_xi[1]),
            0.2,
            np.rad2deg(self.robot.i_xi[2]) + 10,
            np.rad2deg(self.robot.i_xi[2]) - 10,
        )
    )
        
        self.estimated_robot_patch = ax.add_patch(
            Wedge(
                (self.estimated_robot.i_xi[0], self.estimated_robot.i_xi[1]),
                0.2,
                np.rad2deg(self.estimated_robot.i_xi[2]) + 10,
                np.rad2deg(self.estimated_robot.i_xi[2]) - 10,
                facecolor="#00ff0088"
            )
        )
        
    def switch(self):
        self.r_omega*=-1
        
    def update(self):
        
        dt = 0.01
        
        var = 0.01
        M=1000
        
        #r_vel = np.array([self.r_v_dot, 0, self.r_omega], dtype=float)
        phi_dot_l, phi_dot_r = self.robot.r_inverse_kinematics(self.r_v_dot, self.r_omega)
        i_xi_dot = self.robot.forward_kinematics(phi_dot_l, phi_dot_r)
        self.robot.update_state(i_xi_dot, dt)
        #self.robot.update_state_r(r_vel, dt)

        prediction(self.particles, self.r_v_dot, self.r_omega, dt, var)

        self.particles_scatter.set_offsets(self.particles[:, :2])

        self.robot_patch.center = self.robot.i_xi[0], self.robot.i_xi[1]
        self.robot_patch.theta1 = np.rad2deg(self.robot.i_xi[2]) + 10
        self.robot_patch.theta2 = np.rad2deg(self.robot.i_xi[2]) - 10
        self.robot_patch._recompute_path()

        m_res, phi_res,p_m_res,p_phi_res=correction(self.robot, self.landmarks, self.particles, var)

        diff_pos_r_p = np.linalg.norm(m_res - p_m_res, axis=(1, 2))
        diff_or_r_p = np.linalg.norm(phi_res - p_phi_res, axis=1)

        diff = diff_pos_r_p + diff_or_r_p
        x_0 = y_1 = np.min(diff)
        x_1 = y_0 = np.max(diff)
        sim = (y_1 - y_0) / (x_1 - x_0) * (diff - x_0) + y_0
        sim /= np.sum(sim)
        weights = sim
        indices = sampler(weights)
        self.particles = self.particles[indices]

        mean = 1 / M * np.sum(self.particles, axis=0)
        mean[2] = np.arctan2(np.sin(mean[2]), np.cos(mean[2]))
        self.estimated_robot.i_xi = mean

        self.estimated_robot_patch.center = self.estimated_robot.i_xi[0], self.estimated_robot.i_xi[1]
        self.estimated_robot_patch.theta1 = np.rad2deg(self.estimated_robot.i_xi[2]) + 10
        self.estimated_robot_patch.theta2 = np.rad2deg(self.estimated_robot.i_xi[2]) - 10
        self.estimated_robot_patch._recompute_path()
        
        plt.pause(dt)
        self.fig.canvas.draw_idle()
