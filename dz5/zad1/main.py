import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Wedge, Circle
from plot1 import Plot
from shared import R, i_R_r_array, r_R_i_array
from sampler import sampler
from robot import Robot
r_from = -3
r_to = 3



if __name__ == "__main__":
    

    dt = 0.01
    r_v_dot = 0.6
    r_omega = np.pi
    
    r = Robot(np.array([1.7, 1.5, 4 / 3 * np.pi]))


    landmarks = np.random.rand(7, 2)
    landmarks_phi = np.arctan2(landmarks[:, 1], landmarks[:, 0])


    M = 1000
    particles = np.random.randn(M, 3)
    particles[:, 2] = np.arctan2(np.sin(particles[:, 2]), np.cos(particles[:, 2]))
   
    #particles_scatter = ax.scatter(particles[:, 0], particles[:, 1], s=0.5, c="#ff1744")

    var = 0.01

    mean = 1 / M * np.sum(particles, axis=0)
    mean[2] = np.arctan2(np.sin(mean[2]), np.cos(mean[2]))
    er = Robot(mean)

    plot=Plot(r,er,landmarks,particles)

    for i in range(1000):

        plot.update()
        if i%100==0:
            plot.switch()
