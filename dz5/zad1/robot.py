# -*- coding: utf-8 -*-
"""
Created on Sat May  8 22:35:55 2021

@author: David
"""
import numpy as np
from shared import R
class Robot:
    def __init__(self, i_xi=np.array([0, 0, 0], dtype=float)):
        self.i_xi = i_xi
        self.r=0.5
        self.l=0.1

    def update_state(self, i_xi_dot, dt):
        self.i_xi += i_xi_dot * dt
        self.i_xi[2] = np.arctan2(np.sin(self.i_xi[2]), np.cos(self.i_xi[2]))

    def update_state_r(self, r_xi_dot, dt):
        i_xi_dot = R(self.i_xi[2]).transpose() @ r_xi_dot
        self.update_state(i_xi_dot, dt)
    def inverse_kinematics(self, v_dot, omega):
        r=0.05
        l = 0.1
        # pretvoriti transl. brzinu v i kutnu brzinu omega
        # u x_dot, y_dot i omega, i onda to pretvoriti preko
        # invezne kinematike u phi_dots
        phi_dots = 1 / r * np.array([
            [1, 0, -l],
            [1, 0, l],
            [0, 1, 0]
        ], dtype=float) @ R(self.i_xi[2]) @ np.array([
            v_dot * np.cos(self.i_xi[2]),
            v_dot * np.sin(self.i_xi[2]),
            omega
        ], dtype=float)

        phi_dot_l = phi_dots[0]
        phi_dot_r = phi_dots[1]
        return phi_dot_l, phi_dot_r

    def forward_kinematics(self, phi_dot_l, phi_dot_r):

        return R(self.i_xi[2]).transpose() @ np.array(
            [[1 / 2, 1 / 2, 0], [0, 0, 1], [-1 / 2 / self.l, 1 / 2 / self.l, 0]]) @ np.array([self.r * phi_dot_l, self.r * phi_dot_r, 0])
    def r_inverse_kinematics(self, r_v_dot, r_omega):
        phi_dots = 1 / self.r * np.array([
            [1, 0, -self.l],
            [1, 0, self.l],
            [0, 1, 0]
        ], dtype=float) @ np.array([
            r_v_dot, 0, r_omega
        ], dtype=float)

        phi_dot_l = phi_dots[0]
        phi_dot_r = phi_dots[1]
        return phi_dot_l, phi_dot_r