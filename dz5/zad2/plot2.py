# -*- coding: utf-8 -*-
"""
Created on Sat May  8 23:31:35 2021

@author: David
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Wedge, Circle
from robot import Robot
from functions import *

class Plot:
    def __init__(self, robot,estimated_robot, ut, r_from=-3.0, r_to=3.0):
        self.robot=robot
        self.estimated_robot=estimated_robot
        

        self.r_from=r_from
        self.r_to=r_to
        #self.landmarks_phi = np.arctan2(self.landmarks[:, 1], self.landmarks[:, 0])
        self.r_v_dot = ut[0]
        self.r_omega = ut[1]
        plt.ion()
        fig, ax = plt.subplots(1)
    
        ax.set_xlim(self.r_from, self.r_to)
        ax.set_ylim(self.r_from, self.r_to)
        ax.set_aspect("equal")
        ax.set_xticks(np.arange(self.r_from, self.r_to, 0.5))
        ax.set_yticks(np.arange(self.r_from, self.r_to, 0.5))
        ax.set_axisbelow(True)
        plt.grid()
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        self.fig=fig
        self.ax=ax
        

        self.eps=eps

        
        self.robot_patch = ax.add_patch(
        Wedge(
            (self.robot.i_xi[0], self.robot.i_xi[1]),
            0.2,
            np.rad2deg(self.robot.i_xi[2]) + 10,
            np.rad2deg(self.robot.i_xi[2]) - 10,
        )
    )
        
        self.estimated_robot_patch = ax.add_patch(
            Wedge(
                (self.estimated_robot.i_xi[0], self.estimated_robot.i_xi[1]),
                0.2,
                np.rad2deg(self.estimated_robot.i_xi[2]) + 10,
                np.rad2deg(self.estimated_robot.i_xi[2]) - 10,
                facecolor="#00ff0088"
            )
        )
        
        plt.pause(0.01)
        self.fig.canvas.draw_idle()
    def switch(self):
        self.r_omega*=-1
        
    def update(self):
        
        dt = 0.01
        

        phi_dot_l, phi_dot_r = self.robot.r_inverse_kinematics(self.r_v_dot, self.r_omega)
        i_xi_dot = self.robot.forward_kinematics(phi_dot_l, phi_dot_r)
        self.robot.update_state(i_xi_dot, dt)
        
        
        
        self.mi,self.eps=Extended_Kalman_filter(self.estimated_robot.i_xi,self.eps,[self.r_v_dot,self.r_omega],self.robot)
        self.estimated_robot.update_state_es(self.mi,dt)




        self.robot_patch.center = self.robot.i_xi[0], self.robot.i_xi[1]
        self.robot_patch.theta1 = np.rad2deg(self.robot.i_xi[2]) + 10
        self.robot_patch.theta2 = np.rad2deg(self.robot.i_xi[2]) - 10
        self.robot_patch._recompute_path()

        self.estimated_robot_patch.center = self.estimated_robot.i_xi[0], self.estimated_robot.i_xi[1]
        self.estimated_robot_patch.theta1 = np.rad2deg(self.estimated_robot.i_xi[2]) + 10
        self.estimated_robot_patch.theta2 = np.rad2deg(self.estimated_robot.i_xi[2]) - 10
        self.estimated_robot_patch._recompute_path()
        
        plt.pause(dt)
        self.fig.canvas.draw_idle()
