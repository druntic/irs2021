# -*- coding: utf-8 -*-
"""
Created on Sun May  9 23:13:11 2021

@author: David
"""
import numpy as np
from robot import Robot


x0=[0.7, 1.5, 3 / 2 * np.pi]
mi0=[0, 0, 0]
ut=[0.8,np.pi*3/2]
var1=0.05
var2=0.01
delta_t=0.01

eps=np.diag([100,100,100])

def g(xt,ut):
    return np.array([xt[0]+ut[0]*delta_t*np.cos(xt[2]),xt[1]+ut[0]*delta_t*np.sin(xt[2]),xt[2]+ut[1]*delta_t])
def G(xt,ut):
    e=np.matrix([
        [1,0, -delta_t*ut[0]*np.sin(xt[2])],
        [0,1,delta_t*ut[0]*np.cos(xt[2])],
        [0,0,1]
        ])

    return e

Rt=np.diag([var1,var1,var1])

Qt=np.diag([var2,var2])
Ht=np.matrix([
    [1,0,0],
    [0,1,0]
    ])


def Extended_Kalman_filter(mi,eps,ut,robot):
    #xt=x0
    mi_m=g(mi,ut)
    eps_m=G(mi,ut)@eps@G(mi,ut).transpose()+Rt
    z=np.array([robot.i_xi[0]+np.random.normal(0,var1),robot.i_xi[1]+np.random.normal(0,var1)]) 
    Kt=eps_m @ Ht.transpose() @ np.linalg.inv(Ht@eps_m@Ht.transpose()+Qt)
    
    mi=mi_m+(Kt@(z-Ht@mi_m).transpose()).transpose()
    eps=(np.eye(3, dtype=float)-Kt@Ht)@eps_m
    #print("estimated theta:",mi.item(2))
    #print("theta:",robot.i_xi[2])
    return np.array([mi.item(0),mi.item(1),mi.item(2)]), eps