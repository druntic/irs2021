# -*- coding: utf-8 -*-
"""
Created on Sat May  8 22:21:51 2021

@author: David
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Wedge, Circle
from shared import r_R_i_array
from sampler import sampler


def prediction(particles, r_v_dot, r_omega, dt, var):
    M=1000
    r_vel = np.array([r_v_dot, 0, r_omega], dtype=float)

    particles += np.tensordot(
        r_vel,
        r_R_i_array(particles[:, 2]),
        axes=(0, 1)
    ) * dt
    particles += np.random.normal(0, var, (M, 3))
    particles[:, 2] = np.arctan2(np.sin(particles[:, 2]), np.cos(particles[:, 2]))
    

def correction(robot, landmarks, particles, var):
    landmarks_phi = np.arctan2(landmarks[:, 1], landmarks[:, 0])
    M=1000
    m_res = landmarks - robot.i_xi[:2]
    m_res += np.random.normal(0, var, (landmarks.shape[0], 2))
    phi_res = landmarks_phi - robot.i_xi[2]
    phi_res += np.random.normal(0, var, landmarks.shape[0])
    particles_reshaped = np.reshape(particles, (M, 1, 3))
    p_m_res = landmarks - particles_reshaped[:, :, :2]
    p_m_res += np.random.normal(0, var, (M, landmarks.shape[0], 2))
    p_phi_res = landmarks_phi - particles_reshaped[:, :, 2]
    p_phi_res += np.random.normal(0, var, (M, landmarks.shape[0]))
    
    return m_res, phi_res,p_m_res,p_phi_res