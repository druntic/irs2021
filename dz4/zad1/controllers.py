import numpy as np
from pid import PID
from robot import r,phi_max

class GoToGoalController:
    def __init__(self, robot, goal):
        self.goal = goal
        self.robot = robot
        self.pid = PID()

    def __call__(self, dt):
        e = np.arctan2(self.goal.i_g[1] - self.robot.i_xi[1], self.goal.i_g[0] - self.robot.i_xi[0]) - self.robot.i_xi[2]
        e = np.arctan2(np.sin(e), np.cos(e))

        r_omega = self.pid(e, dt)
        #phi_dot_max=200
        r_v_dot = r*phi_max
        r_v_dot_feas = r_v_dot
        r_v_dot_feas,r_omega_feas = self.robot.feas(r_v_dot, r_omega,dt)

        return r_v_dot_feas, r_omega_feas

class StopController:
    def __call__(self, dt):
        return 0, 0