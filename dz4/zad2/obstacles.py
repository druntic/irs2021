import numpy as np
from shared import R

o1_obstacle1_points = np.array([
    [-0.25, -0.25, 0],
    [0.25, -0.25, 0],
    [0.25, 0.25, 0],
    [-0.25, 0.25, 0]
], dtype=float)

i_R_o1 = R(np.pi / 2.2).transpose()
i_t_o1 = np.array([0.75, -0.5, 0], dtype=float)


o2_obstacle2_points = np.array([
    [-0.25, -0.25, 0],
    [0.25, -0.25, 0],
    [0.25, 0.25, 0],
    [-0.25, 0.25, 0]
], dtype=float)

i_R_o2 = R(np.pi / 4).transpose()
i_t_o2 = np.array([-1, 1, 0], dtype=float)

from polygon import Polygon
from quadtree import QuadTree


class Obstacle:
    def __init__(self, points, R, t):
        self.points = points
        self.R = R
        self.t = t

        self.polygon = Polygon(self[:, :2])

    def __getitem__(self, item):
        return (np.dot(self.points, self.R.transpose()) + self.t)[item]

    def get_bounding_rectangle(self):
        x_min = np.min(self[:, 0])
        x_max = np.max(self[:, 0])
        y_min = np.min(self[:, 1])
        y_max = np.max(self[:, 1])
        return x_min, y_min, x_max - x_min, y_max - y_min


if __name__ == "__main__":
    o = Obstacle(o1_obstacle1_points, i_R_o1, i_t_o1)
    q = QuadTree([o])

